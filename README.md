# blog

Un site de blog ecrit en javascript sass

- Créer plusieurs pages (plusieurs bundles webpack)
- Mise en place de Sass
- Utilisation des images
- Création des articles
- Suppression des articles
- Topbar responsive avec menu pour mobile
