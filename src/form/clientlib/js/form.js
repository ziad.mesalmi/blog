import "../scss/form.scss";
import { FormFunction } from "../../../utils/form.functions";
import { Constantes } from "../../../utils/constantes";
import { ServiceCrud } from "../../../utils/service.crud";
import { Commons } from "../../../utils/commons";

const form = document.querySelector("form");
const errorElem = document.querySelector("#errors");
const btnCancel = document.querySelector(".btn-secondary");
let errors = [];
let errorHTML = "";
let errorHTMLWithReduce = "";

const cleanErrorDiv = () => {
  errorElem.innerHTML = "";
  errorHTMLWithReduce = "";
  errorHTML = "";
  errors = [];
};

const formIsValid = (article) => {
  if (
    !article.author ||
    !article.categorie ||
    !article.content ||
    !article.image ||
    !article.title
  ) {
    errors.push(Constantes.messages.error);
  } else {
    cleanErrorDiv();
  }
  if (errors.length) {
    errors.forEach((er) => {
      errorHTML += `<li>${er}</li>`;
    });
    errorElem.innerHTML = errorHTML;
    //   on peut aussi utiliser reduce
    errorHTMLWithReduce = errors.reduce((acc, value) => {
      acc = `<li>${value}</li>`;
      return acc;
    }, "");
    console.log(errorHTMLWithReduce);
    return false;
  } else {
    cleanErrorDiv();
    return true;
  }
};

form.addEventListener("submit", (event) => {
  event.preventDefault();
  cleanErrorDiv();
  const formData = FormFunction.getFormData(form);
  const article = Object.fromEntries(formData.entries());
  if (formIsValid(article)) {
    const jsonArticle = JSON.stringify(article);
    const articleId = getArticleId();
    if (articleId) {
      ServiceCrud.updatePost(articleId, jsonArticle);
    } else {
      ServiceCrud.createPost(jsonArticle);
    }
  }

  // plus d'infos
  //   formData.forEach((value, key, index) => {
  //     console.log(`${key} : ${value}`);
  //   });

  //   for (let entry of formData.entries()) {
  //     console.log(entry);
  //   }
  //   console.log(JSON.stringify(formData));
  //   const array = Array.from(formData.entries());
  //   console.log(array);

  //   // creation d'un objet méthode 1
  //   const arrayObj = Array.from(formData.entries()).reduce((acc, value) => {
  //     acc[value[0]] = value[1];
  //     return acc;
  //   }, {});
  //   console.log(arrayObj);

  //   // creation d'un objet méthode 2
  //   const arrayObjEn = Object.fromEntries(formData.entries());
  //   console.log(arrayObjEn);
});

btnCancel.addEventListener("click", async (event) => {
  const result = await Commons.openConfirmModal(
    Constantes.messages.cancel_update_article,
    Constantes.messages.modals.yesOrNo
  );
  if (result === true) {
    location.assign("/index.html");
  }
});

const fillForm = async (articleId) => {
  const article = await ServiceCrud.readPost(articleId);
  if (article) {
    console.log("mon article: ", article);
    document.querySelector("input[name='author']").value = article.author || "";
    document.querySelector("#image").value = article.image || "";
    document.querySelector("#categorie").value = article.categorie || "";
    document.querySelector("#title").value = article.title || "";
    document.querySelector("#content").value = article.content || "";
  }
};

const checkUpdateMode = () => {
  const articleId = getArticleId();
  if (articleId) {
    fillForm(articleId);
  }
};

const getArticleId = () => {
  const articleId = new URL(location.href).searchParams.get("id");
  return articleId;
};

const initialise = () => {
  checkUpdateMode();
};

initialise();
