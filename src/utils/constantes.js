export const Constantes = {
  network: {
    base_url: "https://restapi.fr/api/",
    url_article_resource: "https://restapi.fr/api/zied-articles",
  },
  messages: {
    error: "Vous devez renseigner tous les champs",
    del_confirm_article: "Etes vous sur de vouloir supprimer cet article?",
    cancel_update_article:
      "Etes vous sur de vouloir annuler vos modifications?",
    modals: {
      yesOrNo: { ok: "Oui", ko: "Non" },
      ConfirmOrCancel: { ok: "Confirmer", ko: "Annuler" },
    },
  },
  http: {
    request_ok_status: 299,
  },
};
