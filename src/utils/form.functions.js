const getFormData = (form) => {
  return new FormData(form);
};

export const FormFunction = { getFormData };
