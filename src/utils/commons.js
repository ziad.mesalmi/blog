const body = document.querySelector("body");
let calc;
let modal;
let eventEscape;
let koBtn;
let okBtn;

const openConfirmModal = (message, btns) => {
  createModal(message, btns);
  createCalc();
  body.append(calc);
  return new Promise((resolve, reject) => {
    calc.addEventListener("click", () => {
      resolve(false);
      calc.remove();
    });
    okBtn.addEventListener("click", () => {
      resolve(true);
      calc.remove();
    });
    koBtn.addEventListener("click", () => {
      resolve(false);
      calc.remove();
    });
  });
};

const createCalc = () => {
  calc = document.createElement("div");
  calc.classList.add("calc");

  calc.append(modal);
};

const createModal = (message, btns) => {
  modal = document.createElement("div");
  modal.classList.add("modal");
  modal.innerHTML = `<p>${message}</p>`;
  modal.addEventListener("click", (event) => event.stopPropagation());
  modal.append(...createBtnModal(btns));
};

const createBtnModal = (btns) => {
  okBtn = document.createElement("button");
  okBtn.classList.add("btn", "btn-primary");
  okBtn.innerText = btns.ok;
  koBtn = document.createElement("button");
  koBtn.classList.add("btn", "btn-secondary");
  koBtn.innerText = btns.ko;
  return [koBtn, okBtn];
};

export const Commons = { openConfirmModal };
