import { Constantes } from "./constantes";

const createPost = async (article) => {
  try {
    const response = await fetch(Constantes.network.url_article_resource, {
      method: "POST",
      body: article,
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.status < Constantes.http.request_ok_status) {
      location.assign("/index.html");
    }
    const body = await response.json();
    console.log(body);
  } catch (error) {
    console.error("error: ", error);
  }
};

const readAllPosts = async (sortBy) => {
  try {
    const response = await fetch(
      `${Constantes.network.url_article_resource}?sort=createdAt:${sortBy}`
    );
    const body = await response.json();
    console.log(body);
    return body;
  } catch (error) {
    console.log("error: ", error);
    return null;
  }
};

const readPost = async (id) => {
  try {
    const response = await fetch(
      Constantes.network.url_article_resource.concat(`/${id}`)
    );
    const body = await response.json();
    console.log(body);
    if (response.status < Constantes.http.request_ok_status) {
      return body;
    }
  } catch (error) {
    console.log("error: ", error);
    return null;
  }
};

const updatePost = async (id, article) => {
  try {
    const response = await fetch(
      Constantes.network.url_article_resource.concat(`/${id}`),
      {
        method: "PATCH",
        body: article,
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (response.status < Constantes.http.request_ok_status) {
      location.assign("/index.html");
    }
    const body = await response.json();
    console.log(body);
  } catch (error) {
    console.error("error: ", error);
  }
};

const deletePost = async (id) => {
  try {
    const response = await fetch(
      Constantes.network.url_article_resource.concat(`/${id}`),
      {
        method: "DELETE",
      }
    );
    const responseBody = await response.json();
    return responseBody;
  } catch (error) {
    console.log("error: ", error);
    return null;
  }
};

const deleteAllPosts = () => {};

export const ServiceCrud = {
  createPost,
  readAllPosts,
  readPost,
  updatePost,
  deletePost,
};
