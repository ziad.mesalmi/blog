const iconeMobile = document.querySelector(".header-menu-icone");
let isMenuIsOpen = false;
let mobileMenuDom = "";
const headerMenu = document.querySelector(".header-menu");

const menuMobile = () => {
  iconeMobile.addEventListener("click", (event) => {
    if (isMenuIsOpen) {
      closeMenu();
    } else {
      openMenu();
    }
    event.stopPropagation();
  });

  // close menu
  window.addEventListener("click", (event) => {
    if (isMenuIsOpen) {
      closeMenu();
    }
  });
  window.addEventListener("resize", (event) => {
    if (window.innerWidth > 480 && isMenuIsOpen) {
      closeMenu();
    }
  });
};

const closeMenu = () => {
  mobileMenuDom.classList.remove("open");
  isMenuIsOpen = false;
};

const openMenu = () => {
  if (!mobileMenuDom) {
    createMobileMenu();
  }
  mobileMenuDom.classList.add("open");
  isMenuIsOpen = true;
};

const createMobileMenu = () => {
  mobileMenuDom = document.createElement("div");
  mobileMenuDom.classList.add("mobile-menu");
  mobileMenuDom.append(headerMenu.querySelector("ul").cloneNode(true));
  headerMenu.append(mobileMenuDom);
  mobileMenuDom.addEventListener("click", (event) => event.stopPropagation());
};

const initialiseTopBar = () => {
  menuMobile();
};

initialiseTopBar();
