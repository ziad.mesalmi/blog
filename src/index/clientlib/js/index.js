import "../scss/index.scss";
import { ServiceCrud } from "../../../utils/service.crud";
import { Constantes } from "../../../utils/constantes";
import { Commons } from "../../../utils/commons";

const articleContainerElem = document.querySelector(".articles-container");
const categoriesElem = document.querySelector(".categories");
const selectElem = document.querySelector("select");
let filter = "all";
let articles;
let sortBy = "desc";

const getArticlesDom = () => {
  const articlesDom = articles
    .filter((article) => {
      if (filter === "all") {
        return true;
      } else {
        return article.categorie === filter;
      }
    })
    .map((article) => {
      const articleDom = document.createElement("div");
      articleDom.setAttribute("data-categorie", article.categorie);
      articleDom.classList.add("article");
      articleDom.innerHTML = `
            <img
              src="${article.image}"
              alt="profile"
            />
            <h2>${article.title}</h2>
            <p class="article-author">${article.author} - ${new Date(
        article.createdAt
      ).toLocaleDateString("fr-FR", {
        weekday: "long",
        day: "2-digit",
        month: "long",
        year: "numeric",
      })}</p>
            <p class="article-content">${article.content}
            </p>
            <div class="article-actions">
              <button class="btn btn-danger" data-my-img="${
                article.image
              }" data-categorie="${article.categorie}" data-post-author="${
        article.author
      }" data-id="${article._id}">Supprimer</button>
              <button class="btn btn-primary" data-id="${
                article._id
              }">Modifier</button>
            </div>
      `;
      return articleDom;
    });
  console.log(articlesDom);
  const [va, ...rest] = articlesDom;
  console.log(...articlesDom);
  console.log(va);
  console.log(rest);
  //   articlesDom.forEach((domElem) => articleContainerElem.appendChild(domElem));
  articleContainerElem.innerHTML = "";
  articleContainerElem.append(...articlesDom);
  addListerners();
};

const getCategoriesMenu = () => {
  // const categoriesDom = articles.map((article) => { });
  const categories = articles.reduce((acc, article) => {
    if (acc[article.categorie]) {
      acc[article.categorie]++;
    } else {
      acc[article.categorie] = 1;
    }
    return acc;
  }, {});
  console.log("categories with reduce", categories);

  // let categoriesWithForEach = {};
  // articles.forEach((article) => {
  //   if (categoriesWithForEach[article.categorie]) {
  //     categoriesWithForEach[article.categorie]++;
  //   } else {
  //     categoriesWithForEach[article.categorie] = 1;
  //   }
  // });
  // console.log("categoriesWithForEach: ", categoriesWithForEach);

  const categoriesArray = Object.keys(categories)
    .map((categorie) => {
      return [categorie, categories[categorie]];
    })
    .sort((c1, c2) => c1[0].localeCompare(c2[0]));
  console.log("categoriesArray ", categoriesArray);
  displayCategorieMenu(categoriesArray);
};

const displayCategorieMenu = (categoriesArray) => {
  const liElem = categoriesArray.map((categorieElem) => {
    const li = document.createElement("li");
    li.innerHTML = `${categorieElem[0]} ( <strong>${categorieElem[1]}</strong> )`;
    if (filter === categorieElem[0]) {
      li.classList.add("active");
    }
    li.addEventListener("click", () => {
      if (filter === categorieElem[0]) {
        filter = "all";
        li.classList.remove("active");
      } else {
        console.log(categorieElem[0]);
        filter = categorieElem[0];
        liElem.forEach((li) => li.classList.remove("active"));
        li.classList.add("active");
      }
      getArticlesDom();
    });
    return li;
  });
  categoriesElem.innerHTML = "";
  categoriesElem.append(...liElem);
};

const getArticles = async () => {
  articles = await ServiceCrud.readAllPosts(sortBy);
  if (articles) {
    getArticlesDom();
    getCategoriesMenu();
  }
};

const addListerners = () => {
  // addDeleteEventListener();
  addDeleteEventListenerWithPopup();
  addUpdateEventListener();
  addSelectEventListener();
};

const addDeleteEventListenerWithPopup = async () => {
  const deleteBtns = articleContainerElem.querySelectorAll(".btn-danger");
  deleteBtns.forEach((delBtn) => {
    delBtn.addEventListener("click", async (event) => {
      const result = await Commons.openConfirmModal(
        Constantes.messages.del_confirm_article,
        Constantes.messages.modals.ConfirmOrCancel
      );
      console.log(result);
      if (result === true) {
        const articleId = event.target.dataset.id;
        const response = await ServiceCrud.deletePost(articleId);
        if (response) {
          getArticles();
        }
      }
    });
  });
};

const addDeleteEventListener = () => {
  // add supprimer event listerner
  const deleteBtns = articleContainerElem.querySelectorAll(".btn-danger");
  deleteBtns.forEach((btn) => {
    btn.addEventListener("click", async (event) => {
      const target = event.target;
      console.log(target);
      const articleId = target.dataset.id;
      console.log(articleId);
      const response = await ServiceCrud.deletePost(articleId);
      if (response) {
        getArticles();
      }
    });
  });
};

const addUpdateEventListener = () => {
  // add update event listerner
  const updateBtns = articleContainerElem.querySelectorAll(".btn-primary");
  updateBtns.forEach((btn) => {
    btn.addEventListener("click", async (event) => {
      const target = event.target;
      console.log(target);
      const articleId = target.dataset.id;
      console.log(articleId);
      location.assign(`/form.html?id=${articleId}`);
    });
  });
};

const addSelectEventListener = () => {
  selectElem.addEventListener("change", () => {
    sortBy = selectElem.value;
    console.log(sortBy);
    getArticles();
  });
};

getArticles();
