const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
  entry: {
    site: path.resolve(__dirname, "src/clientlib.site/js/site.js"),
    main: path.resolve(__dirname, "src/index/clientlib/js/index.js"),
    form: path.resolve(__dirname, "src/form/clientlib/js/form.js"),
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.scss$/i,
        exclude: /node_modules/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([
      {
        from: "./src/clientlib.site/assets/images/*",
        to: "./assets/images",
        flatten: true,
      },
    ]),
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: path.resolve(__dirname, "src/index/index.html"),
      chunks: ["site", "main"],
    }),
    new HtmlWebpackPlugin({
      filename: "form.html",
      template: path.resolve(__dirname, "src/form/form.html"),
      chunks: ["site", "form"],
    }),
  ],
  devtool: "source-map",
  mode: "development",
  devServer: {
    contentBase: path.resolve(__dirname, "./dist"),
    inline: true,
    open: true,
    hot: true,
    port: 4000,
  },
};
